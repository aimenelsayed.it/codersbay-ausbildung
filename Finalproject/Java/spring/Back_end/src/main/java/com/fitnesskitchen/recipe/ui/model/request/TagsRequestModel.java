package com.fitnesskitchen.recipe.ui.model.request;

import com.fitnesskitchen.recipe.io.entity.RecipeEntity;

import java.util.List;

public class TagsRequestModel {
    private String name;
    private List<RecipeEntity> recipes;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeEntity> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeEntity> recipes) {
        this.recipes = recipes;
    }
}
