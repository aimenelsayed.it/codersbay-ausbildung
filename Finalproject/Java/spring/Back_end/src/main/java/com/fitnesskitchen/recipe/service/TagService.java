package com.fitnesskitchen.recipe.service;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import com.fitnesskitchen.recipe.shared.dto.TagsDto;
import com.fitnesskitchen.recipe.shared.dto.UserDto;

import java.util.List;

public interface TagService {

    TagsEntity createTag(TagsEntity tag);

    TagsEntity findByName(String name);
    List<String> listTags();
}
