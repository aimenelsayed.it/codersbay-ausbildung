package com.fitnesskitchen.recipe.shared.dto;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;

import java.util.List;

public class RecipeDTO {
    private String recipeId;
    private long id;

    private String type;
    private String description;
    //////// whiche recipe beloge to user
    private UserDto userDetails;
    private List<TagsEntity> tags;



    ///// set and get ...................   1a>
    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }


    public List<TagsEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagsEntity> tags) {
        this.tags = tags;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }




    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserDto getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDto userDetails) {
        this.userDetails = userDetails;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
