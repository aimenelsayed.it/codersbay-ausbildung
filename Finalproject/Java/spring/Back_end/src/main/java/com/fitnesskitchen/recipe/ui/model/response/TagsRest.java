package com.fitnesskitchen.recipe.ui.model.response;

import com.fitnesskitchen.recipe.io.entity.RecipeEntity;

import java.util.List;

public class TagsRest {


    private String name;
    private List<RecipeEntity> recipes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeEntity> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeEntity> recipes) {
        this.recipes = recipes;
    }
}
