package com.fitnesskitchen.recipe.io.entity;

import com.fitnesskitchen.recipe.shared.dto.UserDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name="recipes")
public class RecipeEntity implements Serializable {

    private static final long serialVersionUID = 6835192601898364280L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length=30 )
    private String recipeId;

    @ManyToMany
    @JoinTable(
        name="recipe_tags",
        joinColumns = {@JoinColumn(name ="recipe_id")},
            inverseJoinColumns = {@JoinColumn(name ="tag_id")}

    )
    private List<TagsEntity> tags;

    @Column(nullable = false, length=30)
    private String type;

    @Column(nullable = false, length=250)
    private String description;

    //////// many recipe beloge to one use
    @ManyToOne
    @JoinColumn(name="users_id")
    private UserEntity userDetails;















    public List<TagsEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagsEntity> tags) {
        this.tags = tags;
    }

    public UserEntity getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserEntity userDetails) {
        this.userDetails = userDetails;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
