package com.fitnesskitchen.recipe.contoller;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import com.fitnesskitchen.recipe.service.TagService;
import com.fitnesskitchen.recipe.service.impl.TagsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping(path = "/tags",
        produces = { MediaType.APPLICATION_JSON_VALUE})
public class TagsController {

    @Autowired
    private TagService tagService;

    @GetMapping
    public List<String> listTags(){
        return tagService.listTags();
    }

}
