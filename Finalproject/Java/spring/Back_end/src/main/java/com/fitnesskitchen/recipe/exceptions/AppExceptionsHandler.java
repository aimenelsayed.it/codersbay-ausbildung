package com.fitnesskitchen.recipe.exceptions;

import com.fitnesskitchen.recipe.ui.model.response.ErrorMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class AppExceptionsHandler {

    ////>>>>>>>>>>>>>>>>>>>>  multi handle exception with 1 methode we add come and comend NullPointerException.class
    @ExceptionHandler(value = {UserServiceException.class })

    //// more genral exception class UserServiceException >> this why we dont need this
    public ResponseEntity<Object> handleUserServiceException(UserServiceException ex, WebRequest request)
    {
        ErrorMessage errorMessage = new ErrorMessage(new Date(), ex.getMessage() );

        //// handle exception
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

///// gernal handle exception
    @ExceptionHandler(value = {Exception.class })

    //// more genral exception class UserServiceException >> this why we dont need this
    public ResponseEntity<Object> handleOtherException(Exception ex, WebRequest request)
    {
        ErrorMessage errorMessage = new ErrorMessage(new Date(), ex.getMessage() );

        //// handle exception
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
