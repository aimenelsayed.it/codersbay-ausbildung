package com.fitnesskitchen.recipe.service;

import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;

import java.util.List;

public interface RecipeService {
//////////////////
   List<RecipeDTO> getRecipes(String userId );

////////
   RecipeDTO getRecipe(String recipeId);




}
