package com.fitnesskitchen.recipe.service.impl;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import com.fitnesskitchen.recipe.io.repositories.TagRepository;
import com.fitnesskitchen.recipe.service.RecipeService;
import com.fitnesskitchen.recipe.service.TagService;
import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;
import com.fitnesskitchen.recipe.shared.dto.TagsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagsServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public TagsEntity createTag(TagsEntity tag) {
        return tagRepository.save(tag);
    }

    @Override
    public TagsEntity findByName(String name) {
        return tagRepository.findByName(name);
    }

    public List<String> listTags(){
        List<String> list=new ArrayList<>();
        tagRepository.findAll().iterator().forEachRemaining(tag->{
            list.add(tag.getName());
        });
        return list;
    }


}
