package com.fitnesskitchen.recipe.io.repositories;

import com.fitnesskitchen.recipe.io.entity.RecipeEntity;
import com.fitnesskitchen.recipe.io.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
///// crud need to provide datatype of auto incremte id Long

@Repository
public interface RecipeRepository extends CrudRepository<RecipeEntity, Long> {
    // findAll because we look for list
    List<RecipeEntity> findAllByUserDetails(UserEntity userEntity);

/// return 1 value
   RecipeEntity findByRecipeId(String recipeId);

}
