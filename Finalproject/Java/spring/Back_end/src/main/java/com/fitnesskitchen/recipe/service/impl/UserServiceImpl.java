package com.fitnesskitchen.recipe.service.impl;

import com.fitnesskitchen.recipe.exceptions.UserServiceException;
import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import com.fitnesskitchen.recipe.io.repositories.UserRepository;
import com.fitnesskitchen.recipe.io.entity.UserEntity;
import com.fitnesskitchen.recipe.service.TagService;
import com.fitnesskitchen.recipe.service.UserService;
import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;
import com.fitnesskitchen.recipe.shared.dto.UserDto;
import com.fitnesskitchen.recipe.shared.dto.UserDtoRegister;
import com.fitnesskitchen.recipe.shared.dto.Utils;
import com.fitnesskitchen.recipe.ui.model.response.ErrorMessage;
import com.fitnesskitchen.recipe.ui.model.response.ErrorMessages;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TagService tagService;

    @Autowired
    Utils utils;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto createUser(UserDto user) {
        //>>>>>>>> to check if there is more email with same value

        for (RecipeDTO recipeDTO : user.getRecipes()) {
            for (int i = 0; i < recipeDTO.getTags().size(); i++) {
                TagsEntity tag = recipeDTO.getTags().get(i);
                TagsEntity tagsEntity = tagService.findByName(tag.getName());
                if (tagsEntity == null) {
                    tagService.createTag(tag);
                } else {
                    recipeDTO.getTags().set(i,tagsEntity);
                  //  recipeDTO.getTags().add(tagsEntity);
                }
            }

        }

      /*  user.getRecipes().forEach(recipe -> {
            recipe.getTags().forEach(tag -> {

            });
        }); */

        if (userRepository.findByEmail(user.getEmail()) != null) throw new RuntimeException("Record already exist");

        ///  lopp so list from Dto
        for (int i = 0; i < user.getRecipes().size(); i++) {
            RecipeDTO recipe = user.getRecipes().get(i);
            recipe.setUserDetails(user);
            // to genrate id for each recipe
            recipe.setRecipeId(utils.generateRecipeId(30));
            // send it back to dto
            user.getRecipes().set(i, recipe);

        }

        // UserEntity userEntity = new UserEntity();
        //>>> Beno use to copy info stored at user dto
        //BeanUtils.copyProperties(user, userEntity);
        ModelMapper modelMapper = new ModelMapper();
        UserEntity userEntity = modelMapper.map(user, UserEntity.class);


        //>>>> to create random userid of legth 30
        String publicUserId = utils.generateUserId(30);
//>>> beause they required we put intial value , now use code
        ////>>>>>>>    to incode password and id
        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userEntity.setUserId(publicUserId);


////>>>>> take userEntity save it to database
        UserEntity storedUserDetails = userRepository.save(userEntity);

        ////>>> now return to dto
        //UserDto returnValue = new UserDto();
        //BeanUtils.copyProperties(storedUserDetails, returnValue);
        UserDto returnValue = modelMapper.map(storedUserDetails, UserDto.class);


        return returnValue;
    }

    ////>>>> implent methode for  UserDto getUser(String email); because we need to get used details from data base
    @Override
    public UserDto getUser(String email) {
        UserEntity userEntity = userRepository.findByEmail(email);
        //>>>> to mae sure email not null
        if (userEntity == null) throw new UsernameNotFoundException(email);
        //>>> create return value <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        UserDto returnValue = new UserDto();
        BeanUtils.copyProperties(userEntity, returnValue);
        return returnValue;
    }

    ////>>>>>>>>>>>>> load user
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email);
////>>>>>>>< to check login is exsit or no in date base
        if (userEntity == null) throw new UsernameNotFoundException(email);
        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
    }


    /////////>>>>>>>>>>>>>>>>>>>>>> to get user by useid
    @Override
    public UserDto getUserByUserId(String userId) {
        UserDto returnValue = new UserDto();

        //// now user repositry to make ftech database to userid
        UserEntity userEntity = userRepository.findByUserId(userId);
        // check if null
        if (userEntity == null) throw new UsernameNotFoundException(userId);
        BeanUtils.copyProperties(userEntity, returnValue);
        return returnValue;

    }
    ////////////////////>>>>>>>>>>>>>>>>>>>>>>> update user

    @Override
    public UserDto updateUser(String userId, UserDto user) {
        UserDto returnValue = new UserDto();
        UserEntity userEntity = userRepository.findByUserId(userId);
        if (userEntity == null) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        UserEntity updatedUserDetails = userRepository.save(userEntity);
        BeanUtils.copyProperties(updatedUserDetails, returnValue);
        return returnValue;
    }


    /////////////////////////////Delete<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void deleteUser(String userId) {
        //// to ge id we want to delet
        UserEntity userEntity = userRepository.findByUserId(userId);
        if (userEntity == null) throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        //// dele provide by sping
        userRepository.delete(userEntity);

    }


    ////////////////>>>>>>> to call list of our coustmer
    @Override
    public List<UserDto> getUsers(int page, int limit) {
        List<UserDto> returnValue = new ArrayList<>();
        if (page > 0) page -= 1;


        // find all pagebale,
        Pageable pageableRequest = PageRequest.of(page, limit);

        Page<UserEntity> usersPage = userRepository.findAll(pageableRequest);
        List<UserEntity> users = usersPage.getContent();
        for (UserEntity userEntity : users) {
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(userEntity, userDto);
            returnValue.add(userDto);
        }
        return returnValue;

    }


    @Override
    public UserDtoRegister createUser(UserDtoRegister user) {

        if (userRepository.findByEmail(user.getEmail()) != null) throw new RuntimeException("Record already exist");

        UserEntity userEntity = new UserEntity();
        //>>> Beno use to copy info stored at user dto
        BeanUtils.copyProperties(user, userEntity);
        //>>>> to create random userid of legth 30


        String publicUserId = utils.generateUserId(30);
//>>> beause they required we put intial value , now use code
        ////>>>>>>>    to incode password and id
        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userEntity.setUserId(publicUserId);
////>>>>> take userEntity save it to database
        UserEntity storedUserDetails = userRepository.save(userEntity);

        ////>>> now return to dto
        UserDtoRegister returnValue = new UserDtoRegister();
        BeanUtils.copyProperties(storedUserDetails, returnValue);


        return returnValue;

    }

}