package com.fitnesskitchen.recipe.service;

import com.fitnesskitchen.recipe.shared.dto.UserDto;
import com.fitnesskitchen.recipe.shared.dto.UserDtoRegister;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

  UserDto createUser(UserDto user);

  //>>>>> i will make methode to return user email for login
  UserDto getUser(String email);

  //>>>>> methode to return user Id
  UserDto getUserByUserId(String userId);


  ///>>> methode to update user
  UserDto updateUser(String userId, UserDto user);

  ///>>> methode to delte user
  void deleteUser(String userId);

  ///>>> methode to call cutomer
  List<UserDto> getUsers(int page, int limit);


  //register
  UserDtoRegister createUser(UserDtoRegister user);

}