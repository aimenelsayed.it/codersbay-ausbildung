/*package com.fitnesskitchen.recipe.service.impl;

import com.fitnesskitchen.recipe.io.entity.UserEntity;
import com.fitnesskitchen.recipe.io.repositories.UserRepository;
import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;
import com.fitnesskitchen.recipe.shared.dto.UserDto;
import com.fitnesskitchen.recipe.shared.dto.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;


    @Mock
    Utils utils;


    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

   String userId = "aimensadfsd";
   String encryptedPassword = "sdfsfsdf23423432";
   UserEntity userEntity;


    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setFirstName("Aimen");
        userEntity.setUserId(userId);
        userEntity.setEncryptedPassword(encryptedPassword);

    }

    @Test
    void getUser() {



       when(userRepository.findByEmail( anyString() )).thenReturn(userEntity);
         UserDto userDto = userService.getUser("test@test.com");

         assertNotNull(userDto);
         assertEquals("Aimen", userDto.getFirstName());

    }

        @Test
    final void testGetUser_UsernameNotFoundException(){

            when(userRepository.findByEmail( anyString() )).thenReturn(null);

             assertThrows(UsernameNotFoundException.class,
                     ()->{
                         userService.getUser("test@test.com");
                     }

                     );

        }


        @Test
    final void testCreateUser(){


            when(userRepository.findByEmail( anyString() )).thenReturn(null);
            when(utils.generateRecipeId(anyInt())).thenReturn("dsfsdfsdfsdf");
            when(utils.generateUserId(anyInt())).thenReturn(userId);
            when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
           // when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);


             RecipeDTO recipeDTO = new RecipeDTO();
             recipeDTO.setType("shipping");


            List<RecipeDTO> recipes = new ArrayList<>();
            recipes.add(recipeDTO);


            UserDto userDto = new UserDto();
            userDto.setRecipes(recipes);

            userService.createUser(userDto);

        }


}*/