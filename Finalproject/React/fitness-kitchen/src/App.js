import HomePage from './pages/homepage/homepage.component';
import { GlobalStyle } from './global.styles';
import { Switch,  Route, Redirect } from 'react-router-dom'
import ShopPage from './pages/Shop/shop.component';
import Header from './components/header/header.component';
import SignInAndSignOutPage from './pages/sign-in-and-sign-out/sign-in-and-sign-out.component';
import React from 'react';
import { connect } from 'react-redux';
import { selectCurrentUser} from './redux/user/user.selectors';
import { createStructuredSelector } from 'reselect';
import CheckoutPage from './pages/checkout/checkout.component';
import { checkUserSession} from './redux/user/user.actions';
import ContactPage from './pages/contact/contact';
import VoucherComponent from './components/Voucher/voucher.component';
class App extends React.Component {
   


  ////>>>>>      save and delete google log in 
  unsubscribeFromAuth = null

  componentDidMount() {
    const { checkUserSession } = this.props;
     checkUserSession();
  }



  ////>>>>>      sign-out
  componentWillUnmount(){
    this.unsubscribeFromAuth();
  }

  render()
  {

    return (
      <div >
        <GlobalStyle/>
        <Header />
        <Switch>
        <Route exact path='/' component={HomePage} />
        <Route  path='/shop' component={ShopPage} />
        <Route exact path='/signin' render={ ()=> this.props.currentUser ? (<Redirect to='/' />) : (<SignInAndSignOutPage /> ) } />
        <Route  exact  path='/checkout' component={CheckoutPage} />
        <Route  exact  path='/contact' component={ContactPage} />
        <Route  exact  path='/voucher' component={VoucherComponent} />
        </Switch>
    
      </div>
    ); 
  }


}

/*const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser
})

const mapDispatchToProps = dispatch =>({
   setCurrentUser: user => dispatch(setCurrentUser(user))
});
*/
/*const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
  collectionArray: selectCollectionsForPreview

})
*/

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser 
  //collectionsArray: selectCollectionsForPreview
})


const mapDispatchToProps =  dispatch => ({
  checkUserSession: () => dispatch(checkUserSession())
});;



export default connect(mapStateToProps,mapDispatchToProps)(App);
