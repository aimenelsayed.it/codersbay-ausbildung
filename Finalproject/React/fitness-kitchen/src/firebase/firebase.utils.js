import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';


////>>>>>>>>>>       firebase connect<
const config ={
        apiKey: "AIzaSyB_mY3RzAB3Wp1kq4Eb2MIK40S6XTLgYGQ",
        authDomain: "fitness-kitchen-f4da2.firebaseapp.com",
        projectId: "fitness-kitchen-f4da2",
        storageBucket: "fitness-kitchen-f4da2.appspot.com",
        messagingSenderId: "694930459959",
        appId: "1:694930459959:web:579ab08f2fe9d811055eb2",
        measurementId: "G-16CZDHDYCF"
      };

////>>>>>>>                 if sign gooogle user ready in or not 
  export const createUserProfileDocument = async (userAuth, additionalData) => {
            if (!userAuth) return;
            const userRef = firestore.doc(`users/${userAuth}`);
            const snapshot = await userRef.get()


            
            if (!snapshot.exists) {
                   const { displayName, email} = userAuth;
                   const createdAt = new Date();

                   try {
                          await userRef.set({
                                 displayName,
                                 email,
                                 createdAt,
                                 ...additionalData
                          })

                   } catch(error){
                          console.log('error creating user', error.message);
                   }
                   
            } 

            return userRef;

  };


  /////>>>>> to add date to date base when we want 

  export const addCollectionAndDocuments = async (collectionKey, objectsToAdd ) => {
         const collectionRef = firestore.collection(collectionKey);



         const batch = firestore.batch(); 
         objectsToAdd.forEach(obj =>  {
                const newDocRef = collectionRef.doc();
                console.log(newDocRef + 'booooooooooo');
                batch.set(newDocRef, obj);
         });
      return await  batch.commit();
  };
       

  export const convertCollectionsSnapshotToMap=(collections) => {
          const transformedCollection = collections.docs.map(doc => {
               const { title, items } = doc.data();

               return {
                      routeName: encodeURI(title.toLowerCase()),
                      id: doc.id,
                      title,
                      items

               }
          });

          return transformedCollection.reduce((accumulator, collection)=>  {
                 accumulator[collection.title.toLowerCase()] = collection;
                 return accumulator;
          }  , {});

          console.log(transformedCollection);
   };
      
      const app = firebase.initializeApp(config);


      

//>>>>>>   relove use >> i dont understnad
export const getCurrentUser = () => {
        return  new Promise((resolve, reject) => {
               const unsubscribe = auth.onAuthStateChanged(userAuth => {
                      unsubscribe(); 
                      resolve(userAuth);

               }, reject);
        });

};



////>>>>>>>>>>       firebase connect to auth and database
       export const auth = firebase.auth();
       export const firestore = firebase.firestore();







////>>>>>>>>>>      Google connect
     export  const  googleProvider = new firebase.auth.GoogleAuthProvider();
       googleProvider.setCustomParameters({ prompt: 'select_account' });
       export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);


       export default firebase;