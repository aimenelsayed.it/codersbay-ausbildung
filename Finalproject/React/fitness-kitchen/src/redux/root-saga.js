import { all, call} from 'redux-saga/effects';
import { shopSagas } from './shop/shop.sagas';
import { userSagas } from './user/user.saga';
import { cartSages } from './cart/cart.sages';


export default function* rootSaga() {
 yield all([
  call(shopSagas),
  call(userSagas),
  call(cartSages)
 ]);

}