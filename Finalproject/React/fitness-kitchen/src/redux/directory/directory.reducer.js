const INITAL_STATE = {
    sections: [
        {
            title: 'Breakfast',
            imageUrl: 'https://cdn.pixabay.com/photo/2019/05/08/12/24/yogurt-4188523_960_720.jpg',
            id: 1,
            linkUrl: 'shop/breakfast'
          },
          {
            title: 'Lunch',
            imageUrl: 'https://images.pexels.com/photos/1860204/pexels-photo-1860204.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            id: 2,
            linkUrl: 'shop/lunch'
          },
          {
            title: 'Dinner',
            imageUrl: 'https://images.pexels.com/photos/2862154/pexels-photo-2862154.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            id: 3,
            linkUrl: 'shop/dinner'
          },
          {
            title: 'Vegetarian',
            imageUrl: 'https://images.pexels.com/photos/1143754/pexels-photo-1143754.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            size: 'large',
            id: 4,
            linkUrl: 'shop/vegetarian'
          },
          {
            title: 'Vegan',
            imageUrl: 'https://images.pexels.com/photos/6546420/pexels-photo-6546420.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            size: 'large',
            id: 5,
            linkUrl: 'shop/vegan'
          }
    ]
}

 const directoryReducer = (state = INITAL_STATE, action) => {
     switch(action.type) {
         default:
             return state;
     }
 }



 export default directoryReducer;