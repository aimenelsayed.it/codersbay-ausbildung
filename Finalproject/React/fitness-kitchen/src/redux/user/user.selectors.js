import { createSelector } from "reselect";


const selectUser = state => state.user;

export const selectCurrentUser = createSelector(
    [selectUser],
    /* FUNCTION GET FIRED */
    (user) => user.currentUser
);