import React from "react";
import './cart-item.styles.scss';

const CartItem = ({ item: { imageUrl, price, name, quantity  }}) => (
    <div className='card-item'>
        <img style={{width: "50%"  ,  height: "70px"  }} src={imageUrl} alt='item' />
        <div className='item-details'>
             <span className='name'>{name} (</span>
             <span className='name'>{quantity} * ${price})</span>
        </div>

    </div>
)



export default CartItem;