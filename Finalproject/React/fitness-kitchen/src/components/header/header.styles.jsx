import styled, {css} from "styled-components";
import { Link } from "react-router-dom";



const OptionsContainerStyles = css`
       padding: 30px 15px;
        cursor: pointer;
        

`;


  export  const HeaderContainer = styled.div`
    height: 70px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-bottom: 25px;

    @media screen and (max-width: 800px) {
      padding: 10px;
      height: 60px;
      margin-bottom: 20px;



  }

    `;


    export const LogoContainer = styled(Link)`
    height: 20%;
    width: 70px;
    padding: 25px;



    .logo {
      height: 60px;
      width:60px
    }
    
    @media screen and (max-width: 800px) {
      width: 30px;
      padding: 0 ;



    `;



    export const OptionsContainer = styled.div`
    width: 70%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: flex-end;



      @media screen and (max-width: 800px) {
        width: 0%;

    `;


    export const OptionLink = styled(Link)`
    ${OptionsContainerStyles}
    
    font-weight:bold;



    @media screen and (max-width: 800px) {
      justifyContent:space-between;
      padding: 15px;
      font-size: 12px;
     

      display: inline;



    `;







    export const OptionDiv = styled.div`
    ${OptionsContainerStyles}
    `;