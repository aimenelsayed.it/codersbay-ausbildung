import reactDom from 'react-dom';
import React, { Component } from 'react'
import Select from 'react-select'
import axios from 'axios';
import '../Voucher/voucher.style.css';

import makeAnimated from 'react-select/animated';
import { useStore } from 'react-redux';


const animatedComponents = makeAnimated();
let Tags = [];

let selectedOptions = [];

class VoucherComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      message: ''
    };

    axios.get("http://localhost:8080/fitnesskitchen_recipe/tags")
      .then(function (response) {
        for (let k = 0; k < response.data.length; k++) {
          Tags[k] = { "value": response.data[k], "label": response.data[k] };
        }
      });
  }


  getInputValue = (inputId) => {
    return document.getElementById(inputId).value
  }

  onCreateRecipes = () => {

    let selectedElements = document.getElementById("tagsList").getElementsByClassName("css-12jo7m5");
    let selectedTags = [];
    for (let x = 0; x < selectedElements.length; x++) {
      selectedTags.push({ name: selectedElements[x].innerText });
    }

    let recipe = {
      firstName: this.getInputValue("firstName"),
      lastName: this.getInputValue("lastname"),
      email: this.getInputValue("email"),
      password: "Not Required",
      recipes: [{
        type: this.getInputValue("type"),
        description: this.getInputValue("description"),
        tags: selectedTags
      }
      ]
    };

    fetch('http://localhost:8080/fitnesskitchen_recipe/users', {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(recipe)
    }).then(r => r.json()).then(res => {
      
      if (res) {
        console.log("test = " + res)
        this.setState({ message: 'New recipe is Created Successfully' });
      }
    });


    

  }




  render() {

    return (
      
          <div class="voucher-main"> 

            <div class="voucher-post">
            <h2 style={{ color: 'red' }} > Share Your Recipe with us and Win Voucher!!</h2>
            <h2> So How this work?</h2>
            <h2>Send us your favorite Recipe!, Our chefs will and make each individual Recipe</h2>
            <h2> At 28 of its Month, You will get invitation by Mail To </h2>
            <h2 style={{ color: 'green' }} >FITNESS-KITCHEN</h2>
            <h2> Where, we will try all these recipes and winner will be </h2>
            <h2 style={{ color: 'green' }} >Announce</h2>
        </div>

        
        
        <div  class="voucher-post">
         
            <h2>Enter Your Recipe  </h2>
        <p>
          <label>FirstName : </label>
          <input type="text" id="firstName" ref="firstName" required></input>
        </p>
        <p>
          <label>Lastname : </label>
          <input type="text" id="lastname" ref="lastName" required></input>
        </p>
        <p>
          <label> Email: </label>
          <input type="text" id="email" ref="email" required></input>
        </p>
         
        <p>
          <label>Recipe (type/name) : </label>
          <input type="text" id="type" ref="type" required></input>
        </p>
        <p>
          <label>Description : </label>
          <input type="text" id="description" ref="description" required></input>
        </p>
        <p>
          <Select id="tagsList"
            closeMenuOnSelect={false}
            components={animatedComponents}
            isMulti
            options={Tags}
          />
        </p>

        <button onClick={this.onCreateRecipes} >Submit</button>

        <p>{this.state.message}</p>
        
        </div>
        









        
        </div>


         


    )
  }
}



export default VoucherComponent;