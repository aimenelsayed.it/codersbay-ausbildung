import { connect } from "react-redux";
import { createSelectorCreator, createStructuredSelector } from "reselect";
import { selectIsCollectionFetching } from "../../redux/shop/shop.selectors";
import WithSpinner from "../with-spinner/with-spinner.component";
import CollectionsOverview from "./collections-overview.component";
//// new

import { compose } from 'redux';

/////>>> to name it AS SPINER excpect >>>>> isLoading 
const mapStateToProps = createStructuredSelector ({
    isLoading: selectIsCollectionFetching
});
 
//>>> wrape CollectionsOverview with  WithSpinner .................. compose
const CollectionsOverviewContainer = compose(
    connect(mapStateToProps),
    WithSpinner
  )(CollectionsOverview);
  
  export default CollectionsOverviewContainer;

