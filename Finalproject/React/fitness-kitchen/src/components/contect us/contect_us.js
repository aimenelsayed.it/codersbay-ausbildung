import React from 'react';
import "./contect_us.css";
import emailjs from 'emailjs-com';
import { useState } from 'react';

const Result =() => {
    return(
        <h3 > Your message has been successfully sent to 
            Fitness-Kitchen will contect you soon!  </h3>
    )
}



function Contect  (props)  {

    const [result, showResult] = useState(false);
    

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_lvtnpru', 'template_lg8b5mz', e.target, 'user_VQHjLe8qUkdmJCQeQzhnI')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
      showResult(true);
  };
    
    
    setTimeout(() =>{
        showResult(false)
    },10000)
    
    
    return (
        
             <div class="contactOverlay">
            <form class="form" action="" onSubmit={sendEmail}>
        <div class="formWord">
          <h2>Contact US</h2>
          <span>Full Name</span>
          <br />
          <input class="input100" type="text" name="from_name" required />
          <br />
          <span>Phone Number</span>
          <br />
          <input class="input100" type="text" name="phone" required />
          <br />
          <span>Enter Email</span>
          <br />
          <input class="input100" type="text" name="email" required />
          <br />
        </div>
        <div class="formWord">
          <span>Message</span>
          <br />
          <textarea name="message" required></textarea>
          <br />
          <button class="A">SUBMIT</button>

          <div class="row">
                    {result ? <Result/> : null }
          </div>
        </div>
      </form>
        </div>
        

        
        
    )
}

export default Contect;
