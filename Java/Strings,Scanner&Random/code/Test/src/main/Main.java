package main;
//Java code to illustrate the use of comparator()
import java.util.Comparator;
import java.util.TreeSet;


public class Main {
	public static void main(String[] args)
	{
		TreeSet<String> tree_set = new TreeSet<String>();
		
		tree_set.add("G");
		tree_set.add("E");
		tree_set.add("E");
		tree_set.add("K");
		tree_set.add("S");
		tree_set.add("4");
		

		System.out.println("The elements sorted "
		);
		for (String element : tree_set)
			System.out.print(element + " ");
	}
}
