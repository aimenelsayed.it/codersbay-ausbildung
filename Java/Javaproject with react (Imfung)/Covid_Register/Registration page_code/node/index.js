const express = require('express')
const app = express();
const port = 4000;
const path = require('path')

app.set('views', path.join(__dirname, '../view'))
app.use(express.static('../view'))



app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, '../view/Info.html'))
})




app.get('/reg', (req, res)=>{
    res.sendFile(path.join(__dirname, '../view/reg.html'))
     
})


app.listen(port, () => {
  console.log(`Server läuft unter http://localhost:${port}`);
})