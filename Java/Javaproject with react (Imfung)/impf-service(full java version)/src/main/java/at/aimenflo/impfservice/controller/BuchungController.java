package at.aimenflo.impfservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.aimenflo.impfservice.entity.Buchung;
import at.aimenflo.impfservice.entity.Impfstoff;
import at.aimenflo.impfservice.entity.Patient;
import at.aimenflo.impfservice.repository.BuchungRepository;
import at.aimenflo.impfservice.repository.ImpfstoffRepository;
import at.aimenflo.impfservice.repository.PatientRepository;

@RestController
@RequestMapping("/api")
public class BuchungController {

    @Autowired
    BuchungRepository buchungRepository;

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    ImpfstoffRepository impfstoffRepository;

//--------------------------------GET ALL BUCHUNGEN START--------------------------------   
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/buchung",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Buchung> getBuchung() {
        List<Buchung> buchungList = buchungRepository.findAll();
        return buchungList;
    }
//--------------------------------GET ALL BUCHUNGEN END--------------------------------   

//--------------------------------GET ALL FREIE BUCHUNGEN START--------------------------------   
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/buchung/frei",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Buchung> getFreieBuchung() {
        List<Buchung> buchungList = buchungRepository.findByFrei(true);
        return buchungList;
    }
//--------------------------------GET ALL FREIE BUCHUNGEN END--------------------------------   

//--------------------------------INSERT BUCHUNG INTO DB START--------------------------------
    @RequestMapping(
        method = RequestMethod.POST,
        path = "/buchung/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Buchung addBuchung(@PathVariable(required = true) String svnr, @RequestBody(required = true) Buchung buchung){
        if (buchung.getImpfstoff() == null) {
            throw new RuntimeException("Buchung hat keinen Impfstoff");
        }
        Optional<Patient> findPatient = patientRepository.findById(svnr);
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person nicht in der Datenbank " + svnr);
        }
        Optional<Buchung> findBuchung = buchungRepository.findById(buchung.getId_buchung());
        if (findBuchung.isEmpty()) {
            throw new RuntimeException("Termin nicht in der Datenbank vorhanden " + buchung.getId_buchung());
        }
        Buchung buchungdb = findBuchung.get();
        if (!buchungdb.getFrei()) {
            throw new RuntimeException("Termin ist nich Frei " + buchungdb.getId_buchung());
        }
        buchungdb.setPatient(findPatient.get());
        buchungdb.setFrei(false);
        buchungdb = buchungRepository.save(buchungdb);

        Impfstoff impfstoff = buchung.getImpfstoff();
        impfstoff.setBuchung(buchungdb);
        impfstoffRepository.save(impfstoff);
        buchungdb.setImpfstoff(impfstoff);
        return buchungdb;
    }
//--------------------------------INSERT BUCHUNG INTO DB END--------------------------------

//--------------------------------DELETE BUCHUNG FROM PATIENT START--------------------------------
     @RequestMapping(
        method = RequestMethod.DELETE,
        path = "/buchung/{svnr}/{id_buchung}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Buchung delBuchung(@PathVariable(required = true) String svnr, @PathVariable(required = true) Integer id_buchung){
        Optional<Patient> findPatient = patientRepository.findById(svnr);
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person nicht in der Datenbank " + svnr);
        }
        Patient patient = findPatient.get();
        for (Buchung buchungdb : patient.getBuchung()) {
            if (buchungdb.getId_buchung().equals(id_buchung)) {
                buchungdb.setFrei(true);
                buchungdb.setPatient(null);
                buchungdb = buchungRepository.save(buchungdb);
                Impfstoff impfstoff = buchungdb.getImpfstoff();
                impfstoffRepository.delete(impfstoff);
                buchungdb.setImpfstoff(null);
                return buchungdb;
            }
        }
        throw new RuntimeException("Keine Buchung für dienen Patienten gefunden " + id_buchung);
    }
//--------------------------------DELETE BUCHUNG FROM PATIENT END--------------------------------
}