package at.aimenflo.impfservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Impfstoff;

public interface ImpfstoffRepository extends JpaRepository<Impfstoff, Integer>  {
    
}
