package at.aimenflo.impfservice.entity;

import java.util.Date;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class  Person {

    private String vorname, nachname, email, festnetznr;
    private Date geburtsdatum;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Person(){

    }

    public Person(String vorname, String nachname, String email, String festnetznr, Date geburtsdatum) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.festnetznr = festnetznr;
        this.geburtsdatum = geburtsdatum;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFestnetznr() {
        return festnetznr;
    }

    public void setFestnetznr(String festnetznr) {
        this.festnetznr = festnetznr;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }
//--------------------------------GET/SET END--------------------------------

//--------------------------------ABSTRACT METHOD START--------------------------------
    public abstract void test();
//--------------------------------ABSTRACT METHOD END--------------------------------   
}