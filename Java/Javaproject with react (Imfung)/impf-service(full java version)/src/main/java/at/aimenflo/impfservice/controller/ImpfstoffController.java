package at.aimenflo.impfservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.aimenflo.impfservice.entity.Impfstoff;
import at.aimenflo.impfservice.repository.ImpfstoffRepository;

@RestController
@RequestMapping("/api")
public class ImpfstoffController {
    
    @Autowired
    ImpfstoffRepository impfstoffRepository;

//--------------------------------GET ALL IMPFSTOFF START--------------------------------    
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/impfstoff",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Impfstoff> getImpfstoff() {
        List<Impfstoff> impfstoffList = impfstoffRepository.findAll();
        return impfstoffList;
    }
//--------------------------------GET ALL IMPFSTOFF END--------------------------------   
}