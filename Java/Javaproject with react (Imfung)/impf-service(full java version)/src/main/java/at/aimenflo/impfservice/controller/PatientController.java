package at.aimenflo.impfservice.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.aimenflo.impfservice.entity.Mitarbeiter;
import at.aimenflo.impfservice.entity.Patient;
import at.aimenflo.impfservice.repository.PatientRepository;

@RestController
@RequestMapping("/api")
public class PatientController {
    Logger logger = LoggerFactory.getLogger(PatientController.class);
    @Autowired
    PatientRepository patientRepository;

//--------------------------------GET ALL PATIENTS FROM DB START--------------------------------
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/patient",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Patient> getPatient(){
        logger.info("Enter getPatient");
        List<Patient> patientList  = patientRepository.findAll();
        return patientList;
    }
//--------------------------------GET ALL PATIENTS FROM DB END--------------------------------

//--------------------------------GET ONE PATIENT BY SVNR START--------------------------------
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/patient/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient getPatientBySvnr(@PathVariable String svnr){
        logger.info("Enter getPatientBySvnr", svnr);
        Optional<Patient> patient  = patientRepository.findById(svnr);
        if (patient.isPresent()) {
            return patient.get();    
        }
        return null;
    }
//--------------------------------GET ONE PATIENT BY SVNR END--------------------------------

//--------------------------------GET PATIENTS BY FIRSTNAME/LASTNAME START-------------------------------- 
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/patient/find/{nachname}/{vorname}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Patient> getPatientByNachnameVorname(@PathVariable String nachname, @PathVariable String vorname){
        
        List<Patient> findPatienten  = patientRepository.findByNachnameAndVorname(nachname, vorname);
        if (findPatienten.isEmpty()) {
            throw new RuntimeException("Person wurde nicht gefunden." + nachname + " " + vorname);
        }
        return findPatienten;
    }
//--------------------------------GET PATIENTS BY FIRSTNAME/LASTNAME END-------------------------------- 

//--------------------------------INSERT PATIENT INTO DB START-------------------------------- 
    @RequestMapping(
        method = RequestMethod.POST,
        path = "/patient/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient addPatient(@PathVariable String svnr, @RequestBody Patient patient){
        patient.setSvnr(svnr);
        Optional<Patient> findPatient = patientRepository.findById(patient.getSvnr());
            if (findPatient.isPresent()) {
                throw new RuntimeException("Person schon in der Datenbank " + patient.getSvnr());
            }
        patient = patientRepository.save(patient);
        return patient;
    }
//--------------------------------INSERT PATIENT INTO DB END-------------------------------- 

//--------------------------------UPDATE PATIENT IN DB START-------------------------------- 
    @RequestMapping(
        method = RequestMethod.PUT,
        path = "/patient/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient updatePatient(@PathVariable String svnr, @RequestBody Patient patient){
        patient.setSvnr(svnr);
        Optional<Patient> findPatient = patientRepository.findById(patient.getSvnr());
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person wurde nicht gefunden." + patient.getSvnr());
        }
        patient = patientRepository.save(patient);
        return patient;
    }
//--------------------------------UPDATE PATIENT IN DB END-------------------------------- 

//--------------------------------DELETE PATIENT FROM DB START-------------------------------- 
    @RequestMapping(
        method = RequestMethod.DELETE,
        path = "/patient/{svnr}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Patient> deletePatient(HttpServletRequest request, @PathVariable String svnr){
        Mitarbeiter user = (Mitarbeiter)request.getSession().getAttribute("loginUser");
        if (!user.isVorgesetzt()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN.value()).build();
        }
        Optional<Patient> findPatient = patientRepository.findById(svnr);
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person wurde nicht gefunden." + svnr);
        }
        patientRepository.delete(findPatient.get());
        return ResponseEntity.ok().build();
    }
//--------------------------------DELETE PATIENT FROM DB END-------------------------------- 
}