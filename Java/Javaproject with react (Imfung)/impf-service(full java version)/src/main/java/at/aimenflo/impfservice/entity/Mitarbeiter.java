package at.aimenflo.impfservice.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "mitarbeiter")
public class Mitarbeiter extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_mitarbeiter", nullable=false)
    private Integer id_mitarbeiter;

    @Column(nullable = false)
    private String team, passwort;
    
    @Column(nullable = false)
    private boolean vorgesetzt;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Mitarbeiter(){
        super();
    }

    public Mitarbeiter(String vorname, String nachname, String email, String festnetznr, Date geburtsdatum, String team, String passwort, Integer id_mitarbeiter, boolean vorgesetzt) {
        super(vorname, nachname, email, festnetznr, geburtsdatum);
        this.team = team;
        this.passwort = passwort;
        this.id_mitarbeiter = id_mitarbeiter;
        this.vorgesetzt = vorgesetzt;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public Integer getId_mitarbeiter() {
        return id_mitarbeiter;
    }

    public void setId_mitarbeiter(Integer id_mitarbeiter) {
        this.id_mitarbeiter = id_mitarbeiter;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public boolean isVorgesetzt() {
        return vorgesetzt;
    }

    public void setVorgesetzt(boolean vorgesetzt) {
        this.vorgesetzt = vorgesetzt;
    }
//--------------------------------GET/SET END--------------------------------

//--------------------------------ABSTRACT METHOD CALL START--------------------------------
    @Override
    public void test() {

    }
//--------------------------------ABSTRACT METHOD CALL END--------------------------------
}