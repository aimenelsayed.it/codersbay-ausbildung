package at.aimenflo.impfservice.controller;

import at.aimenflo.impfservice.entity.Mitarbeiter;
import at.aimenflo.impfservice.repository.MitarbeiterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/")
public class LoginController {
    
    @Autowired
    MitarbeiterRepository mitarbeiterRepository;

//--------------------------------MITARBEITER LOGIN START--------------------------------
    @RequestMapping(
        method = RequestMethod.POST,
        path = "/login"
    )
    public ResponseEntity<Object> login(HttpServletRequest request, @RequestParam(required = true) String userid, @RequestParam(required = true) String password){

        Mitarbeiter mitarbeiter = mitarbeiterRepository.findByEmail(userid);
        if (mitarbeiter == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        //TODO: Password hash
        String passdbHash = mitarbeiter.getPasswort();
        if (!passdbHash.equals(password)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        request.getSession().setAttribute("loginUser", mitarbeiter);
        return ResponseEntity.ok().build();
    }
//--------------------------------MITARBEITER LOGIN END--------------------------------

//--------------------------------MITARBEITER LOGOUT START--------------------------------
    @RequestMapping(
        method = RequestMethod.POST,
        path = "/logout"
    )
    public ResponseEntity<Object> logout(HttpServletRequest request){
        request.getSession().invalidate();
        return ResponseEntity.ok().build();
    }
//--------------------------------MITARBEITER LOGIN END--------------------------------
}