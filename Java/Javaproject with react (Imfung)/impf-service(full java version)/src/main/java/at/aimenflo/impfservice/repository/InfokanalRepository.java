package at.aimenflo.impfservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Infokanal;

public interface InfokanalRepository extends JpaRepository<Infokanal, Integer> {



}
