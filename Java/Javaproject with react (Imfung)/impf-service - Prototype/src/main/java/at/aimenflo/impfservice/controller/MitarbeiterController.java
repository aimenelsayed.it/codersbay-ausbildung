package at.aimenflo.impfservice.controller;

import at.aimenflo.impfservice.entity.Mitarbeiter;
import at.aimenflo.impfservice.repository.MitarbeiterRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MitarbeiterController {
    
    @Autowired
    MitarbeiterRepository mitarbeiterRepository;

    @RequestMapping(
        method = RequestMethod.GET,
        path = "/mitarbeiter",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Mitarbeiter> getMitarbeiter(){

        List<Mitarbeiter> mitarbeiterList  = mitarbeiterRepository.findAll();
        return mitarbeiterList;
    }
}
