package at.aimenflo.impfservice.controller;

import at.aimenflo.impfservice.entity.Buchung;
import at.aimenflo.impfservice.repository.BuchungRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BuchungController {

    @Autowired
    BuchungRepository buchungRepository;

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/buchung",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public List<Buchung> getBuchung() {

        List<Buchung> buchungList = buchungRepository.findAll();
        return buchungList;
    }
}