package at.aimenflo.impfservice.controller;

import at.aimenflo.impfservice.entity.Patient;
import at.aimenflo.impfservice.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class PatientController {

    @Autowired
    PatientRepository patientRepository;

    //Get all Patients from DB
    @RequestMapping(
            method = RequestMethod.GET,
            path = "/patient",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Patient> getPatient(){

        List<Patient> patientList  = patientRepository.findAll();
        return patientList;

    }

    //Get one Patient by svnr
    @RequestMapping(
            method = RequestMethod.GET,
            path = "/patient/{svnr}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient getPatientBySvnr(@PathVariable String svnr){

        Optional<Patient> patient  = patientRepository.findById(svnr);
        if (patient.isPresent()) {
            return patient.get();
        }
        return null;

    }

    //Get Persons by Firstname and Lastname
    @RequestMapping(
            method = RequestMethod.GET,
            path = "/patient/find/{nachname}/{vorname}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Patient> getPatientByNachnameVorname(@PathVariable String nachname, @PathVariable String vorname){

        List<Patient> patienten  = patientRepository.findByNachnameAndVorname(nachname, vorname);
        return patienten;

    }

    //-----------------------------------Add Patient into
    @RequestMapping(
            method = RequestMethod.POST,
            path = "/patient",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
       public Patient addPatient(@RequestBody Patient patient){

        Optional<Patient> findPatient = patientRepository.findById(patient.getSvnr());

        patient = patientRepository.save(patient);
        return patient;

    }

    //--------------------------------------Update Patient
    @RequestMapping(
            method = RequestMethod.PUT,
            path = "/patient/{svnr}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient updatePatient(@PathVariable String svnr, @RequestBody Patient patient){
        patient.setSvnr(svnr);
        Optional<Patient> findPatient = patientRepository.findById(patient.getSvnr());
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person wurde nicht gefunden." + patient.getSvnr());
        }
        patient = patientRepository.save(patient);

        return patient;

    }

    //------------------------------------------------Delete Patient
    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "/patient/{svnr}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Patient deletePatient(@PathVariable String svnr){
        Optional<Patient> findPatient = patientRepository.findById(svnr);
        if (findPatient.isEmpty()) {
            throw new RuntimeException("Person wurde nicht gefunden." + svnr);
        }
        patientRepository.delete(findPatient.get());

        return null;

    }

}
