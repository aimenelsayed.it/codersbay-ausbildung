package at.aimenflo.impfservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Buchung;

public interface BuchungRepository extends JpaRepository<Buchung, Integer> {
}
