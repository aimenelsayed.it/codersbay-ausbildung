package at.aimenflo.impfservice.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "patient")
public class Patient extends Person {

    @Id
    @Column(name="svnr", nullable=false)
    private String svnr;
    
    //TODO try out
    @NotNull
    private String mobilnr, strasse, hausnr, stiege, tuer, plz, ort;

    @NotNull
    @Enumerated(EnumType.STRING)
    private GeschlechtEnum geschlecht;
    public static enum GeschlechtEnum{
        M, W, D
    }

   // @OneToOne(mappedBy = "patient")
   // private Buchung buchung;


    //---------------------------------------- super call and extranl variable ----------------------
    public Patient() {
        super();
    }

    public Patient(String vorname, String nachname, String email, String festnetznr, Date geburtsdatum, String svnr,
            String mobilnr, String strasse, String hausnr, String stiege, String tuer, String plz, String ort,
            GeschlechtEnum geschlecht) {
        super(vorname, nachname, email, festnetznr, geburtsdatum);
        this.svnr = svnr;
        this.mobilnr = mobilnr;
        this.strasse = strasse;
        this.hausnr = hausnr;
        this.stiege = stiege;
        this.tuer = tuer;
        this.plz = plz;
        this.ort = ort;
        this.geschlecht = geschlecht;
    }



    //--------------------------------set and get -----------------------------------------------------
    public String getSvnr() {
        return svnr;
    }

    public void setSvnr(String svnr) {
        this.svnr = svnr;
    }

    public String getMobilnr() {
        return mobilnr;
    }

    public void setMobilnr(String mobilnr) {
        this.mobilnr = mobilnr;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnr() {
        return hausnr;
    }

    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    public String getStiege() {
        return stiege;
    }

    public void setStiege(String stiege) {
        this.stiege = stiege;
    }

    public String getTuer() {
        return tuer;
    }

    public void setTuer(String tuer) {
        this.tuer = tuer;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    
    public GeschlechtEnum getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(GeschlechtEnum geschlecht) {
        this.geschlecht = geschlecht;
    }

   /* public Buchung getBuchung() {
        return buchung;
    }

    public void setBuchung(Buchung buchung) {
        this.buchung = buchung;
    }*/
     //----------------------------- To string--------------------------------------


    @Override
    public String toString() {
        return "Patient{" +
                "svnr='" + svnr + '\'' +
                ", mobilnr='" + mobilnr + '\'' +
                ", strasse='" + strasse + '\'' +
                ", hausnr='" + hausnr + '\'' +
                ", stiege='" + stiege + '\'' +
                ", tuer='" + tuer + '\'' +
                ", plz='" + plz + '\'' +
                ", ort='" + ort + '\'' +
                ", geschlecht=" + geschlecht +
               // ", buchung=" + buchung +
                '}';
    }

    //------------------------ abstract methode call-----------------------------------
    @Override
    public void test() {
        // TODO Auto-generated method stub
        
    }       
}
