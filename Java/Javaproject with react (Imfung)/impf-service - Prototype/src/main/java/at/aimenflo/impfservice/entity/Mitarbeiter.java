package at.aimenflo.impfservice.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "mitarbeiter")
public class Mitarbeiter extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_mitarbeiter", nullable=false)
    private Integer id_mitarbeiter;

    @NotNull
    private String team, passwort;
    
    @NotNull
    private boolean vorgesetzt;



    //---------------------------------------- super call and extranl variable ----------------------

    public Mitarbeiter(){
        super();
    }

    public Mitarbeiter(String vorname, String nachname, String email, String festnetznr, Date geburtsdatum, String team, String passwort, Integer id_mitarbeiter, boolean vorgesetzt) {
        super(vorname, nachname, email, festnetznr, geburtsdatum);
        this.team = team;
        this.passwort = passwort;
        this.id_mitarbeiter = id_mitarbeiter;
        this.vorgesetzt = vorgesetzt;
    }



    //--------------------------------set and get -----------------------------------------------------

    public Integer getId_mitarbeiter() {
        return id_mitarbeiter;
    }

    public void setId_mitarbeiter(Integer id_mitarbeiter) {
        this.id_mitarbeiter = id_mitarbeiter;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public boolean isVorgesetzt() {
        return vorgesetzt;
    }

    public void setVorgesetzt(boolean vorgesetzt) {
        this.vorgesetzt = vorgesetzt;
    }





    //------------------------ abstract methode call-----------------------------------
    @Override
    public void test() {

    }
}
