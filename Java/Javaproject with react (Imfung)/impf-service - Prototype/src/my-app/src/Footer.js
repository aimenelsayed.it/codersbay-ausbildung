import { Button } from 'antd';
import { Container } from './Container';
import React from 'react';
import './Footer.css';
import Avatar from 'antd/lib/avatar/avatar';

const Footer = (props) => (
    <div className='footer'>
       
       
        <Container>

               {props.numberOfPatients ? <Avatar 
               style={{backgroundColor: '#f56a00', marginRight: '5px'}}
                size='large'>{props.numberOfPatients}</Avatar> : null}
            <Button onClick={ () => props.handleAddPatientClickEvent()} type='primary'>Add new Patient</Button>
        </Container>
    </div>
);
export default Footer;