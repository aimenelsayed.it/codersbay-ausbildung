import fetch from 'unfetch';

//--------   capture Error
const checksStatus = response =>{
    if(response.ok){
        return response;
    }else{
        let error = new Error(response.statusText);
        error.response = response;
        response.json().then(e => {
            error.error = e;
        });
        return Promise.reject(error);
    }
}

//--------   call patient database
export const getAllPatient = ()=> 
fetch('http://localhost:8080/patient').then(checksStatus);

//--------   ADD patient database
export const addNewPatient = patient => 
fetch('http://localhost:8080/patient', {
    headers: {
        'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(patient)
}).then(checksStatus);
//--------   Edit patient database

export const updatePatient = (svnr, patient) => 
    fetch(`http://localhost:8080/patient/${svnr}`, {
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'PUT',
        body: JSON.stringify(patient)
    })
    .then(checksStatus);





//--------   Delet patient database


export const deletePatient = svnr =>
    fetch(`http://localhost:8080/patient/${svnr}`, {
        method: 'DELETE'
    })
    .then(checksStatus);
