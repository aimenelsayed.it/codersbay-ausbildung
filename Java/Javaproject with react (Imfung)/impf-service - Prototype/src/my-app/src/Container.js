import React from 'react';
export const Container = props => (
    <div style={{width: '1400px', margin: '10px 0', textAlign: 'Center'}} >
        {props.children}

    </div>
);

export default Container;