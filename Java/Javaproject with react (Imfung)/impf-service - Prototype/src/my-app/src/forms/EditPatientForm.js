import React,  {Component} from 'react';
import { Formik } from 'formik';
import { Input, Tag, Button } from 'antd';

export default class EditUserForm extends Component {   
    render () {
        const { submitter, initialValues } = this.props;
        return (
          <Formik
          initialValues={initialValues}
          validate={values => {
            let errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = 'Invalid email address';
            }
           
            if (!values.vorname) {
              errors.vorname = 'vorname required';
            }
            if (!values.nachname) {
              errors.nachname = 'nachname required';
            }
            if (!values.festnetznr) {
                errors.festnetznr = 'festnetznr required';
              }
              if (!values.geburtsdatum) {
                errors.geburtsdatum = 'geburtsdatum required';
              }
              if (!values.svnr) {
                errors.svnr = 'svnr required';
              }
              if (!values.mobilnr) {
                errors.mobilnr = 'mobilnr required';
              }
              if (!values.strasse) {
                errors.strasse = 'strasse required';
              }

              if (!values.hausnr) {
                errors.hausnr = 'hausnr required';
              }
              if (!values.stiege) {
                errors.stiege = 'stiege required';
              }
              if (!values.tuer) {
                errors.tuer = 'tuer required';
              }
              if (!values.plz) {
                errors.plz = 'plz required';
              }
              if (!values.ort) {
                errors.ort = 'ort required';
              }
              if (!values.geschlecht) {
                errors.geschlecht = 'geschlecht required';
              }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values)
            submitter(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            isValid,
            handleBlur,
            handleSubmit,
            isSubmitting,
            submitForm
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <Input
                style={{marginBottom: '5px'}}
                name="vorname"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.vorname}
              />
              {errors.vorname && touched.vorname && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.vorname}</Tag>}

              <Input
                style={{marginBottom: '5px'}}
                name="nachname"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.nachname}
              />
              {errors.nachname && touched.nachname && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.nachname}</Tag>}

              <Input
                style={{marginBottom: '5px'}}
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              {errors.email && touched.email && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.email}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="festnetznr"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.festnetznr}
              />
              {errors.festnetznr && touched.festnetznr && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.festnetznr}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="geburtsdatum"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
              />
              {errors.geburtsdatum && touched.geburtsdatum && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.geburtsdatum}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="svnr"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.svnr}
              />
              {errors.svnr && touched.svnr && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.svnr}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="mobilnr"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.mobilnr}
              />
              {errors.mobilnr && touched.mobilnr && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.mobilnr}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="strasse"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.strasse}
              />
              {errors.strasse && touched.strasse && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.strasse}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="hausnr"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.hausnr}
              />
              {errors.hausnr && touched.hausnr && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.hausnr}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="stiege"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.stiege}
              />
              {errors.stiege && touched.stiege && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.stiege}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="tuer"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.tuer}
              />
              {errors.tuer && touched.tuer && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.tuer}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="plz"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.plz}
              />
              {errors.plz && touched.plz && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.plz}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="ort"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.ort}
              />
              {errors.ort && touched.ort && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.ort}</Tag>}
              <Input
                style={{marginBottom: '5px'}}
                name="geschlecht"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.geschlecht}
              />
              {errors.geschlecht && touched.geschlecht && <Tag style={{marginBottom: '5px'}} color="#f50">{errors.geschlecht}</Tag>}

              <Button onClick = {() => submitForm()} type="submit" disabled={isSubmitting | (touched && !isValid) }>
                Submit
              </Button>
            </form>
          )}
        </Formik>
      )
    }
}

