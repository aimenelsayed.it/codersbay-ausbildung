package fahrzeuge;

public class LKW extends Fahrzeug {




	//.............................super call extends..............................................
	public LKW(int id, int marke, int baujahr, double grundpreis) {
		super( id, marke, baujahr, grundpreis );


	}


	/* -----------------------------------getRabbate section---------------------- */
	@override
	public  double getRabatt( ) {
		double rabatt =  (2021 - super.getBaujahr()) * 0.05;

		if(rabatt > 0.15)
			rabatt = 0.15;

		return rabatt;

	}

	/* ------------------------final price section-------------------------- */
	@override
	public  double preiseNachRabatt() {
		double preis = super.getGrundpreis() - (super.grundpreis * getRabatt());
		return preis;
	}

	//----------------------------print---------------------

	@override

	public void print() {
		System.out.println("id:  " + super.getId() + "Marke: " + super.getMarke());
	}





}