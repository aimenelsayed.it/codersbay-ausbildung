package fahrzeuge;

public abstract class Fahrzeug {

	private int id;
	private String marke;
	protected int baujahr;
	protected double grundpreis;


	//--------------------------super call ---------------------------


	public Fahrzeug(int id, String marke, int baujahr, double grundpreis) {
		super();
		this.id = id;
		this.marke = marke;
		this.baujahr = baujahr;
		this.grundpreis = grundpreis;
	}




	/* -----------------------------------getRabbate section---------------------- */

	public abstract double getRabatt();

	/* ------------------------final price section------------------------------------------ */
	public abstract double preiseNachRabatt();


	/* ------------------------print------------------------------------------ */
	public abstract void print();


	// --------------------------- get and set------------------------------


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarke() {
		return marke;
	}

	public void setMarke(String marke) {
		this.marke = marke;
	}

	public int getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public double getGrundpreis() {
		return grundpreis;
	}

	public void setGrundpreis(double grundpreis) {
		this.grundpreis = grundpreis;
	}
}





