package fahrzeuge;

public class PKW extends Fahrzeug {

	private  int servicejahr; //  imp


	/* ....................super cell.................................. */

	public PKW(int id, String marke, int baujahr, double grundpreis, int servicejahr) {
		super(id, marke, baujahr, grundpreis);
		this.servicejahr = servicejahr;
	}


	/*	................getRabbate section----------------------------- */
	@override
	public  double getRabatt() {
		double rabatt =  (2021 - super.getBaujahr()) * 0.05 + (2021 - servicejahr) * 0.02;

		if(rabatt > 0.1)
			rabatt = 0.1;

		return rabatt;

	}





	/*.............. final price section-------------------------------------------- */
	@override
	public  double preiseNachRabatt() {
		double preis = super.getGrundpreis() - (super.grundpreis * getRabatt());
		return preis;

	}


	@override
	public void print() {
		System.out.println("ID:" + super.getId() + "Marke: " + super.getMarke());
	}


	/*-------------------servicejahr  ( set and get )-----------------------------------*/
	public int getServicejahr() {
		return servicejahr;
	}





	public void setServicejahr(int servicejahr) {
		if(servicejahr >2021) {
			System.out.println("this car will come from future");
		}else if (servicejahr < super.getBaujahr()) {
			System.out.println("service was befor it even exist!!!!");
		}else {
			this.servicejahr = servicejahr;

		}
	}




}


