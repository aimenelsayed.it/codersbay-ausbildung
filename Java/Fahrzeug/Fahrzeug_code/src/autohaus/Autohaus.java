package autohaus;

import java.util.List;

import fahrzeuge.PKW;
import fahrzeuge.LKW;

public class Autohaus {
	private int parkPlatz;
	private boolean parkFull = false;
	private String name;
	private List<Object> fahrzeugeList;


	//....................................................................................

	public Autohaus(int parkPlatz, boolean parkFull, String name, List<Object> fahrzeugeList) {
		super();
		this.parkPlatz = parkPlatz;
		this.parkFull = parkFull;
		this.name = name;
		this.fahrzeugeList = fahrzeugeList;
	}

	//..................................check worker enought or not............................................

	public boolean  zuVielFahrzeuge(int arbeiter) {
		if(arbeiter * 3 < fahrzeugeList.size()) {
			parkFull = true;
		}
		else {
			parkFull = false ;
		}
		return parkFull;
	}

//........................................check place is full or no.......................................



	public boolean  zuVielFahrzeuge() {
		if(fahrzeugeList.size() > parkPlatz) {
			parkFull = true;
		}
		else {
			parkFull = false ;
		}
		return parkFull;
	}

//--------------------------print----------------------------------------------


	public  void ausgabeautohaus() {
		System.out.println("~ ~ ~ <Name des Autohauses> ~ ~ ~");
	}


//.................. call fahrzeug..............................




	public void addFahrzeuge(String fahrzeugTyp, int id, String marke, int baujahr, double grundpreis, int servicejahr) {
		if(fahrzeugTyp.equals("PKW")) {
			fahrzeugeList.add(new PKW(id, baujahr, servicejahr, marke, grundpreis));
		}else if(fahrzeugTyp.equals("LKW")) {
			fahrzeugeList.add(new LKW(id, baujahr, marke, grundpreis));
		}else {
			System.out.println("we dont have this type of car dude!....");
		}

	}



//........................... set and get ...................................

	public int getParkPlatz() {
		return parkPlatz;
	}

	public void setParkPlatz(int parkPlatz) {
		this.parkPlatz = parkPlatz;
	}

	public boolean isParkFull() {
		return parkFull;
	}

	public void setParkFull(boolean parkFull) {
		this.parkFull = parkFull;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Object> getFahrzeugeList() {
		return fahrzeugeList;
	}

	public void setFahrzeugeList(List<Object> fahrzeugeList) {
		this.fahrzeugeList = fahrzeugeList;
	}



}


