package main;

import java.util.NavigableSet;

import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		

		random r = new random();

		hashset hashset = new hashset();
		treeset treeset = new treeset();
		linkedhashset linkedset = new linkedhashset();

		// start time
		long starttime = system.nanotime();
		for (int i = 0; i < 1000; i++) {
		int x = r.nextint(1000 - 10) + 10;
		hashset.add(new dog(x));
		}
		// end time
		long endtime = system.nanotime();
		long duration = endtime - starttime;
		system.out.println("hashset: " + duration);

		// start time
		starttime = system.nanotime();
		for (int i = 0; i < 1000; i++) {
		int x = r.nextint(1000 - 10) + 10;
		treeset.add(new dog(x));
		}
		// end time
		endtime = system.nanotime();
		duration = endtime - starttime;
		system.out.println("treeset: " + duration);

		// start time
		starttime = system.nanotime();
		for (int i = 0; i < 1000; i++) {
		int x = r.nextint(1000 - 10) + 10;
		linkedset.add(new dog(x));
		}
		// end time
		endtime = system.nanotime();
		duration = endtime - starttime;
		system.out.println("linkedhashset: " + duration);

			
		
	}

}
