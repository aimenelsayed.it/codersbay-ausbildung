const express = require('express')
const path = require('path')
const hbs = require('hbs')
const mysql = require('mysql')
const dotenv = require('dotenv')
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser')  
const router = express.Router()
const authController = require('../controllers/auth')





const app = express()

////////////--------endgine and view------------
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '../sign-in'))
hbs.registerPartials(path.join(__dirname, '../sign-in/partials'))
dotenv.config({path: './.env'})
app.use(express.json())


//-/////////----set static diretory--------------------------------
app.use(express.static('../src/opening'))
app.use(express.static('../src/home'))
app.use(express.static('../src/city'))
app.use(express.static('../src/studio_youtube'))
app.use(express.static('../sign-in'))

/////////////-------------body parser----------
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

//------------------------------------Mysql connect------------------------------------------------
//-------------------------------------create link -------------------
/*
const db = mysql.createConnection({
    host : process.env.DaTABASE_HOST,
    user: process.env.DaTABASE_USER,
    password: process.env.DaTABASE_password,
    database: process.env.DaTABASE,

    
})*/

var db = mysql.createConnection({   
    host: 'localhost',
    database: 'genshin',
    user: 'root',
    password: ''
});
/////////------------------- check connect methode---------
db.connect((error)=>{
    if(error){
        console.log(error)
    } else {
        console.log('data base connect')
    }
})


//-//////////----------- auth -----------------
app.use('/auth', require('../routes/auth'))
////////---------------parse--------------------
app.use(express.urlencoded({ extended: false}))
//////////---------jasone form ---------------
app.use(express.json());
app.use(cookieParser())
///////////--------login----------------------

app.get("/login", (req,res)=>{
    res.render(path.join(__dirname, '../sign-in/login.hbs'))
} )

app.get("/register", (req,res)=>{
    res.render(path.join(__dirname, '../sign-in/register.hbs'))
} )


//////////--------------------------logout----------

app.get('/logout', authController.logout)

///////////----------------Profile----------

app.get("/profile", authController.isLoggedIN, (req,res)=> {
    
      if(req.user){
        res.render(path.join(__dirname, '../sign-in/profile.hbs'),
        {
           user : req.user
        })
      }
    else {
        res.render(path.join(__dirname, '../sign-in/login.hbs'))
    } 
})
//------------------ Mail page-------------
app.use(express.static('../send_mail'))

app.get('/mail', (req, res)=>{
    res.sendFile(path.join(__dirname, '../send_mail/mail.html'))
     console.log(req.body)
})
//////////---------------mail JS------







////////////////------setup static path---------
app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, '../src/opening/index.html'))
     
})



app.get('/home', (req, res)=>{
    res.sendFile(path.join(__dirname, '../src/home/index.html'))
})




app.get('/cities', (req, res)=>{
    res.sendFile(path.join(__dirname, '../src/city/city.html'))
     
})



app.get('/videos', (req, res)=>{
    res.sendFile(path.join(__dirname, '../src/studio_youtube/video.html'))
     
})



app.get('/chrachter', (req, res)=>{
    res.sendFile(path.join(__dirname, '../src/chrachter/characters.html'))
     
})



//////////////------------ catch Error------

app.get('/help', (req, res)=>{
    res.render(path.join(__dirname, '../sign-in/404.hbs'), {
        text: 'I SUCK at Node.js , so I cant help you DUDE , JUST GO BACK HOME !!!!!!!!!!!!!!'

    })
})




app.get('*', (req,res)=>{
  res.render(path.join(__dirname, '../sign-in/404.hbs'), {
   text: 'Error DUDE 404 , Try HELP'
  
})

})

/////////////////----------------------server start--------
app.listen(3000, ()=>{
    console.log('server start on port 3000.')
})