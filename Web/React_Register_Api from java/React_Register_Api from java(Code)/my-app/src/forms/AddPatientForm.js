import React from 'react';
import { Formik } from 'formik';
import { Input, Button } from 'antd';
import { addNewPatient } from '../client';

const inputBottomMargin = {marginBottom: '10px'};


const AddPatientForm = (props) =>  (
 //////------------------------------- Add Patient Form
     <Formik
       initialValues={{ vorname: '', nachname: '' , email: '', festnetznr: '', geburtsdatum: '', svnr: '', mobilnr: '', strasse: '', hausnr: '', stiege: '', tuer: '', plz: '', ort: '', geschlecht: ''}}
       validate={values => {
         const errors = {};
         if (!values.vorname) {
            errors.vorname = 'Required vorname';
          } 
          if (!values.nachname) {
            errors.nachname = 'Required nachname';
          } 
         if (!values.email) {
           errors.email = 'Required email';
         } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = 'Invalid email address';
          }
         if (!values.festnetznr) {
            errors.festnetznr = 'Required festnetznr';
          } 
          if (!values.geburtsdatum) {
            errors.geburtsdatum = 'Required geburtsdatum';
          } 
          if (!values.svnr) {
            errors.svnr = 'Required svnr';
          } 
          if (!values.mobilnr) {
            errors.mobilnr = 'Required mobilnr';
          } 
          if (!values.strasse) {
            errors.strasse = 'Required strasse';
          } 
          if (!values.hausnr) {
            errors.hausnr = 'Required hausnr';
          } 
          if (!values.stiege) {
            errors.stiege = 'Required stiege';
          } 
          if (!values.tuer) {
            errors.tuer = 'Required tuer';
          } 
          if (!values.plz) {
            errors.plz = 'Required plz';
          } 
          if (!values.ort) {
            errors.ort = 'Required ort';
          } 
          if (!values.geschlecht) {
            errors.geschlecht = 'Required geschlecht';
          } 
         
         return errors;
       }}
       onSubmit={(patient, { setSubmitting }) => {
           addNewPatient(patient).then(() => {
             props.onSuccess();
             setSubmitting(false);
           })
           
        
       }}
     >
       {({
         values,
         errors,
         touched,
         handleChange,
         handleBlur,
         handleSubmit,
         isSubmitting,
         submitForm,
         isValid
         /* and other goodies         Add what we need in patient table */
       }) => (
         <form onSubmit={handleSubmit}>
           <Input
           style={inputBottomMargin}
             name="vorname"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.vorname}
             placeholder='Vorname'
           />
           {errors.vorname && touched.vorname && errors.vorname}
           <Input
           style={inputBottomMargin}
             name="nachname"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.nachname}
             placeholder='Nachname'
           />
           {errors.nachname && touched.nachname && errors.nachname}
           <Input
           style={inputBottomMargin}
             name="email"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.email}
             placeholder='Email'
           />
           {errors.email && touched.email && errors.email}
           <Input
           style={inputBottomMargin}
             name="festnetznr"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.festnetznr}
             placeholder='Festnetznr'
           />
           {errors.festnetznr && touched.festnetznr && errors.festnetznr}
           <Input
           style={inputBottomMargin}
             name="geburtsdatum"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.geburtsdatum}
             placeholder='Geburtsdatum. eg: 2021-02-02 '
           />
           {errors.geburtsdatum && touched.geburtsdatum && errors.geburtsdatum}
           <Input
           style={inputBottomMargin}
             name="svnr"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.svnr}
             placeholder='svnr '
           />
           {errors.svnr && touched.svnr && errors.svnr}
           <Input
           style={inputBottomMargin}
             name="mobilnr"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.mobilnr}
             placeholder='Mobilnr'
           />
           {errors.mobilnr && touched.mobilnr && errors.mobilnr}
           <Input
           style={inputBottomMargin}
             name="strasse"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.strasse}
             placeholder='Strasse'
           />
           {errors.strasse && touched.strasse && errors.strasse}
           <Input
           style={inputBottomMargin}
             name="hausnr"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.hausnr}
             placeholder='Hausnr'
           />
           {errors.hausnr && touched.hausnr && errors.hausnr}
           <Input
           style={inputBottomMargin}
             name="stiege"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.stiege}
             placeholder='Stiege'
           />
           {errors.stiege && touched.stiege && errors.stiege}
           <Input
           style={inputBottomMargin}
             name="tuer"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.tuer}
             placeholder='Tuer'
           />
           {errors.tuer && touched.tuer && errors.tuer}
           <Input
           style={inputBottomMargin}
             name="plz"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.plz}
             placeholder='Plz'
           />
           {errors.plz && touched.plz && errors.plz}
           <Input
           style={inputBottomMargin}
             name="ort"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.ort}
             placeholder='Ort'
           />
           {errors.ort && touched.ort && errors.ort}
           <Input
           style={inputBottomMargin}
             name="geschlecht"
             onChange={handleChange}
             onBlur={handleBlur}
             value={values.geschlecht}
             placeholder='Geschlecht. eg: M , D'
           />
           {errors.geschlecht && touched.geschlecht && errors.geschlecht}
           
           <Button onClick={() => submitForm()}type="submit" disabled={isSubmitting | (touched && !isValid)}>
             Submit
           </Button>
         </form>
       )}
     </Formik>

        );
    


export default AddPatientForm;