
import { Component, Fragment } from 'react';
import Container from './Container';
import Footer from './Footer';
import './App.css';
import {
  getAllPatient,
  updatePatient,
  deletePatient
} from './client';
//import {getAllPatient} from './client';
import{
  Table,
  Spin,
  Icon,
  Modal,
  Empty,
  PageHeader,
  Button,
  notification, 
  Popconfirm,
  
} from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import { LoadingOutlined } from '@ant-design/icons';
import AddPatientForm from './forms/AddPatientForm';
import EditPatientForm from './forms/EditPatientForm';
import { errorNotification } from './Notifications';
const getIndicatortIcon = () => <LoadingOutlined style={{ fontSize: 24 }} spin />;

//////------------------------------- fetch Table patient 
class App extends Component{
 
//////------------------------------- state control
    state = {
      patients: [],
      isFetching: false,
      selectedPatient:{},
      isAddPatientModalVisisble: false,
      isEditOatientModalVisible: false,
    }
    
    componentDidMount () {
      this.fetchPatients();
    }
//////-------------------------------  control Modal
    openAddPatientModal = ()=> this.setState({isAddPatientModalVisisble: true})
    closeAddPatientModal = ()=> this.setState({isAddPatientModalVisisble: false})
    openEditPatientModal = () => this.setState({ isEditPatientModalVisible: true })
  closeEditPatientModal = () => this.setState({ isEditPatientModalVisible: false })
  openNotificationWithIcon = (type, message, description) => notification[type]({message, description});

//////-------------------------------  control Fetch    
    fetchPatients = () => {
      this.setState({
        isFetching: true
      });

 //////------------------------------- fetch Patient    
      getAllPatient()
      .then(res => res.json()
      .then(patients => {
        console.log(patients);
        this.setState({
          patients,
          isFetching: false
        });   
       }))
//////------------------------------- Read error 
       .catch(error => {
         const message = error.error.message;
         const description = error.error.error;
         errorNotification(message, message)
         this.setState({
           isFetching: false
         });
       });
    }
//////------------------------------- Eidt Patient 

editUser = selectedPatient => {
  this.setState({ selectedPatient });
  this.openEditPatientModal();
}



//////------------------------------- Update Patient 


updatePatientFormSubmitter = patient => {
  updatePatient(patient.svnr, patient).then(() => {
    this.openNotificationWithIcon('success', 'Patient updated', `${patient.svnr} was updated`);
    this.closeEditPatientModal();
    this.fetchPatients();
  }).catch(err => {
    console.error(err.error);
    this.openNotificationWithIcon('error', 'error', `(${err.error.status}) ${err.error.error}`);
  });
}


//////------------------------------- Delete Patient 


deletePatient = svnr => {
  deletePatient(svnr).then(() => {
    this.openNotificationWithIcon('success', 'Patient deleted', `${svnr} was deleted`);
    this.fetchPatients();
  }).catch(err => {
    this.openNotificationWithIcon('error', 'error', `(${err.error.status}) ${err.error.error}`);
  });
}



 //////------------------------------- Render Table patient 
    render(){
      const {patients, isFetching, isAddPatientModalVisisble } = this.state;



      const commonElements = () => (
        <div>
          <Modal
            title='Add new patient'
            visible={isAddPatientModalVisisble}
            onOk={this.closeAddPatientModal}
            onCancel={this.closeAddPatientModal}
            width={1000}>
            <AddPatientForm 
              onSuccess={() => {
                this.closeAddPatientModal(); 
                this.fetchPatients();
              }}
              onFailure={(error) => {
                const message = error.error.message;
                const description = error.error.httpStatus;
                errorNotification(message, description);
              }}
            />
          </Modal>
  
          <Modal
            title='Edit'
            visible={this.state.isEditPatientModalVisible}
            onOk={this.closeEditPatientModal}
            onCancel={this.closeEditPatientModal}
            width={1000}>
            
            <PageHeader title={`${this.state.selectedPatient}`}/>
            
            <EditPatientForm 
              initialValues={this.state.selectedPatient} 
              submitter={this.updatePatientFormSubmitter}/>
          </Modal>
  
          <Footer
            numberOfPatients={patients.length}
            handleAddPatientClickEvent={this.openAddPatientModal}
          />  
        </div>
      )

      //////------------------------------- Fetching
      
      if(isFetching) {
        return <Container>
          <Spin indicator={getIndicatortIcon}/>
        </Container>
      };

      if (patients && patients.length){
           
        const colums = [
  //////------------------------------- ADD prop to table 
          { 
            title:'',
            key: 'avater',
            render: (text, patient) =>(
              <Avatar size='large' >
                  {`${patient.vorname.charAt(0).toUpperCase()}${patient.nachname.charAt(0).toUpperCase()}`}
              </Avatar>
            )
          },
          {
            title: 'svnr',
            dataIndex: 'svnr',
            key: 'svnr'
           },
           {
            title: 'vorname',
            dataIndex: 'vorname',
            key: 'vorname'
           },
           {
            title: 'nachname',
            dataIndex: 'nachname',
            key: 'nachname'
           },
           {
            title: 'email',
            dataIndex: 'email',
            key: 'email'
           },
           {
            title: 'festnetznr',
            dataIndex: 'festnetznr',
            key: 'festnetznr'
           },
           {
            title: 'geburtsdatum',
            dataIndex: 'geburtsdatum',
            key: 'geburtsdatum'
           },
           {
            title: 'mobilnr',
            dataIndex: 'mobilnr',
            key: 'mobilnr'
           },
           {
            title: 'strasse',
            dataIndex: 'strasse',
            key: 'strasse'
           },
           {
            title: 'hausnr',
            dataIndex: 'hausnr',
            key: 'hausnr'
           },
           {
            title: 'stiege',
            dataIndex: 'stiege',
            key: 'stiege'
           },
           {
            title: 'tuer',
            dataIndex: 'tuer',
            key: 'tuer'
           },
           {
            title: 'plz',
            dataIndex: 'plz',
            key: 'plz'
           },
           {
            title: 'ort',
            dataIndex: 'ort',
            key: 'ort'
           }, {
            title: 'geschlecht',
            dataIndex: 'geschlecht',
            key: 'geschlecht'
           },
           {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <Fragment>
                <Popconfirm
                  placement='topRight'
                  title={`Are you sure to delete ${record.svnr}`} 
                  onConfirm={() => this.deletePatient(record.svnr)} okText='Yes' cancelText='No'
                  onCancel={e => e.stopPropagation()}>
                  <Button type='danger' onClick={(e) => e.stopPropagation()}>Delete</Button>
                </Popconfirm>
                <Button style={{marginLeft: '5px'}} type='primary' onClick={() => this.editUser(record)}>Edit</Button>
              </Fragment>
            ),
          }

        ];

      

  return (
    <Container style={ {backgroundImage: "url('https://www.vienna.at/2021/03/ABD0072-20210303-16-9-034840962304-640x360.jpg')"}}>
      <Table 
        style={{marginBottom: '100px'}}
        dataSource={patients} 
        columns={colums} 
        pagination={false}
        rowKey='svnr'/>
      {commonElements()}
    </Container>
  );

}

return (
  <Container>
    <Empty description={
      <h1>No Patients found</h1>
    }/>
    {commonElements()}
  </Container>
)
}
}


export default App;
