const left = document.querySelector('.left')
const right = document.querySelector('.right')
const container = document.querySelector('.container')

left.addEventListener('mouseenter', () => container.classList.add('hover-left'))
left.addEventListener('mouseleave', () => container.classList.remove('hover-left'))

right.addEventListener('mouseenter', () => container.classList.add('hover-right'))
right.addEventListener('mouseleave', () => container.classList.remove('hover-right')) 


const splash = document.querySelector('.splash')
 document.addEventListener('DOMContentLoaded', (e) => {
     setTimeout(() =>{
         splash.classList.add('display-none')
     }, 5000)
 })
