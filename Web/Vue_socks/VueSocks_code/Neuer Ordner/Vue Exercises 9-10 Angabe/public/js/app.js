
let vm = new Vue({
  el: '#app',
  data: {
    title:'Endangered Birds',
    birds: []
  },
  methods: {
    async getBirds() {
      const respone = await fetch('http://localhost:3000/birds');
      this.birds = await respone.json();
    },
    getBirdsAuthors(bird) {
    let output = [];
        for(let i in bird.attributes) {
          output.push(bird.attributes[i].author);
        }
        return output.join(', ');
      }
    
    
  }
});
